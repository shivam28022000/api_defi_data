from fastapi import FastAPI
import os
from pydantic import BaseModel
import pymongo
import requests
from pycoingecko import CoinGeckoAPI
import pandas as pd
import json
from bson.json_util import dumps

app = FastAPI()





@app.get("/tvl_chart")
def tvlChart():
    # Connect to the MongoDB database
    client = pymongo.MongoClient("mongodb+srv://solvendo:12jJ12%40%40%40%40@solvendo.ncednvf.mongodb.net/?retryWrites=true&w=majority")
    db = client["defi_lending"]

    # Create a new collection
    collection = db["Tvl_Chart"]

    data = requests.get("https://api.llama.fi/charts").json()
    # Insert the list of dictionaries into the collection
    print(data)
    collection.insert_many(data)

    return {"Status": "Data inserted/updated into Tvl_Chart collection "}


@app.get("/data_from_coingecko")
def data_from_coingecko():
    cg = CoinGeckoAPI()
    l = []
    for i in range(113):
        df = pd.DataFrame(cg.get_coins_markets(vs_currency='usd',per_page=250,page=i))
        l.append(df) 
    main_data = pd.concat(l)
    data = main_data.to_dict('records')
    client = pymongo.MongoClient("mongodb+srv://solvendo:12jJ12%40%40%40%40@solvendo.ncednvf.mongodb.net/?retryWrites=true&w=majority")
    db = client["defi_lending"]
    collection = db["data_from_coingecko"]
    collection.insert_many(data)
    return {"Status": "Data inserted/updated into data_from_coingecko collection "}


@app.get("/get_coingecko_data")
def get_coingecko_data():
    myclient = pymongo.MongoClient("mongodb+srv://solvendo:12jJ12%40%40%40%40@solvendo.ncednvf.mongodb.net/?retryWrites=true&w=majority")
    mydb = myclient["defi_lending"]
    mycol = mydb["data_from_coingecko"]
    # data_json = MongoJSONEncoder().encode(list(mycol.find({},{"_id":0})))
    json_data = dumps(list(mycol.find({},{"_id":0})))
    return  json_data


@app.get("/get_Tvl_Chart")
def get_Tvl_Chart():
    myclient = pymongo.MongoClient("mongodb+srv://solvendo:12jJ12%40%40%40%40@solvendo.ncednvf.mongodb.net/?retryWrites=true&w=majority")
    mydb = myclient["defi_lending"]
    mycol = mydb["Tvl_Chart"]
    # data_json = MongoJSONEncoder().encode(list(mycol.find({},{"_id":0})))
    json_data = dumps(list(mycol.find({},{"_id":0})))
    return  json_data